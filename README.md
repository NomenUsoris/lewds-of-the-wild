# Lewds of the Wild

Lewds of the Wild is a community driven project intending to modify The Legend of Zelda: Breath of the Wild game, specifically focusing on the dialogue and text entries in order to create a more sensual and humorous experience.

As such, many of the allusions and innuendos in the game will be expanded on, and characters will be given more overt kinks, personalities and interactions with the player character, in order to create a richer, unfiltered experience for the player. 

In particular, the mod currently aims to change the dialogue of NPCs, with several characters outright flirting with the player or making sexually suggestive remarks about their appearance or actions. Moreover, the main character arcs will be greatly expanded upon and diaries will be rewritten to feature their inner thoughts and fantasies. Item descriptions are also planned to be modified for humorous effect.

In the future, when the development cycle is more mature and the suite of modding tools allows for the appropriate level of customization, we would like to change the world of Hyrule into a dating simulator of sorts, with characters keeping track of your interactions with them which in turn affect what they think of you and how interested you are in each other. This may then expand into a fully fledged total conversion, with the player being able to flirt with, give gifts, take characters on dates and even have sexual relations with them (in much the same vein as Bioware’s Dragon Age).
